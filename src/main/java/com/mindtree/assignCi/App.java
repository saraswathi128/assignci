package com.mindtree.assignCi;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World! change to trigger CI using webhook" );
        System.out.println( "Another change to trigger CI using webhook" );
        System.out.println( "Modified webhook url - added bitbucket-hook" );
    }
}
